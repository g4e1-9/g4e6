//GUIA MANEJO STRINGS EJERCICIO 6
//El usuario ingresa una palabra.
//Determinar qué letra aparece mayor cantidad de veces.
//Para simplificar el problema, trabaje solo con mayúsculas.

#include <stdio.h>

int main()
{
    char A[20] = {0}; //Declaro las variables.
    int letras[26] = {0};
    int contador1 = 0;
    int contador2 = 0;
    
    printf("Ingrese una palabra: "); //Solicito el ingreso de una palabra.
    scanf("%s", A);
    
    for (int i = 0; i <= 19; i++) //Resto 65, ya que las mayúsculas en el abecedario ASCII empiezan desde el 65, a cada letra para conseguir su posición en el abecedario y sumo 1 a su posición cada vez que aparece.
    {
        letras[(A[i]-65)]++;
    }
    
    for (int i = 0; i <= 24; i++)
    {
        if (letras[i] > contador1)
        {
            contador1 = letras[i];
            contador2 = i+65;
        }
    }
    
    printf("\nLa letra mas repetida es: %c", contador2); //Muestro en pantalla la letra más repetida en la palabra.
    
    return 0;
}